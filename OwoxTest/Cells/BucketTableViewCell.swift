//
//  BucketTableViewCell.swift
//  OwoxTest
//
//  Created by Lora Kucher on 12/2/18.
//  Copyright © 2018 Lora Kucher. All rights reserved.
//

import UIKit

class BucketTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView?
    @IBOutlet weak var nameLbl: UILabel?
    @IBOutlet weak var oldPriceLbl: UILabel?
    @IBOutlet weak var actualPriceLbl: UILabel?
    @IBOutlet weak var stepperView: UIView?
    
    var sView: StepperView = StepperView()
    
    override func configure(_ data: AnyObject) {
        let product: Product = data as? Product ?? Product()
        product.observeChanges()
        sView = StepperView.configure(product: product)
        contentView.layer.addBorder(edge: .bottom, color: .lightGray, thickness: 0.5)
        stepperView?.clipsToBounds = true
        stepperView?.addSubview(sView)
    }

}
