//
//  ProductList.swift
//  OwoxTest
//
//  Created by Lora Kucher on 12/6/18.
//  Copyright © 2018 Lora Kucher. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

typealias SuccessHandler = ((Any) -> Void)?

class ProductList: Object {
    
    var products = List<Product>()
    
    static func allProducts() -> [Product] {
        let realm = try? Realm()
        guard let array = realm?.objects(Product.self) else { return [] }
        return Array(array)
    }
    
    static func summ(_ completion: SuccessHandler = nil) {
        let realm = try? Realm()
        let summ: Int = realm?.objects(Product.self).sum(ofProperty: "price") ?? 0
        completion?(summ)
    }
    
    static func count(_ completion: SuccessHandler = nil) {
        let realm = try? Realm()
        let count: Int = realm?.objects(Product.self).sum(ofProperty: "count") ?? 0
        completion?(count)
    }
    
    func add(_ product: Product, completion: SuccessHandler = nil) {
        if product.isInvalidated == true { return }
        for one in products {
            if one.isInvalidated == true {
                return
            }
        }
        writeBlock { _ in
            if products.filter({ $0.id == product.id }).first != nil {
                product.count += 1
                product.price = product.price * product.count
            }
            products.append(product)
            completion?(true)
        }
    }
    
    func productList(with dictionary: [String: Any]) -> Product {
        if let one = dictionary["product"] as? [String: Any] {
            return getProduct(with: one)
        }
        return getProduct(with: dictionary)
    }
    
    private func getProduct(with dictionary: [String: Any]) -> Product {
        let pr = Product()
        pr.id = dictionary["id"] as? Int ?? 0
        pr.name = dictionary["title"] as? String
        pr.price = Int(dictionary["price"] as? String ?? "0") ?? 0
        pr.priceForOne = Int(dictionary["price"] as? String ?? "0") ?? 0
        pr.oldPrice = dictionary["old_price"] as? String
        pr.imageURL = dictionary["image"] as? String
        writeBlock { realm in
            realm.add(pr, update: true)
        }
        return pr
    }
    
    func removeAll() {
        writeBlock { realm in
            realm.deleteAll()
        }
    }
    
}

