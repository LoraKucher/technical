//
//  Product.swift
//  OwoxTest
//
//  Created by Lora Kucher on 12/3/18.
//  Copyright © 2018 Lora Kucher. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

class Product: Object {
    
    @objc dynamic var name: String? = nil
    @objc dynamic var price: Int = 0
    @objc dynamic var priceForOne: Int = 0
    @objc dynamic var oldPrice: String? = nil
    @objc dynamic var imageURL: String? = nil
    @objc dynamic var count: Int = 1
    @objc dynamic var id: Int = 0
    @objc private var tag: Int = 1
    var token: NotificationToken? = nil
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    var counted: Int {
        set {
            writeBlock { _ in
                count = newValue
            }
        }
        get {
            return count
        }
    }
    
    var countedPrice: Int {
        set {
            writeBlock { _ in
                price = newValue
            }
        }
        get {
            return price
        }
    }
    
    func observeChanges() {
        onceObserve()
    }
    
    private func onceObserve() {
        token = observe { changes in
            switch changes {
            case .error(let error):
                print(error)
                break
            case .deleted:
                break
            case .change(let propperties):
                for property in propperties {
                    if property.name == "count" || property.name == "price" {
                        NotificationCenter.default.post(name: .updateUI, object: nil, userInfo: nil)
                        return
                    }
                }
                break
            }
        }
    }

}
