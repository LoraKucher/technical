//
//  Notification+Ex.swift
//  OwoxTest
//
//  Created by Lora Kucher on 12/3/18.
//  Copyright © 2018 Lora Kucher. All rights reserved.
//

import UIKit

extension Notification.Name {

    static let updateUI = Notification.Name("updateUI")

}
