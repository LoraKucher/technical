//
//  String+Ex.swift
//  OwoxTest
//
//  Created by Lora Kucher on 12/6/18.
//  Copyright © 2018 Lora Kucher. All rights reserved.
//

import UIKit

extension String {
    
     func returnCrossedText() -> NSMutableAttributedString {
        let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
        return attributeString
    }
    
}
