//
//  UIViewController+Ex.swift
//  OwoxTest
//
//  Created by Lora Kucher on 12/4/18.
//  Copyright © 2018 Lora Kucher. All rights reserved.
//

import UIKit

extension UIViewController {
        
    func addActivity() {
        if isActivityOn == true {
            return
        }
        let activity = ActivityView.configure(with: self.view.frame.size)
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.view.addSubview(activity)
            self.view.bringSubviewToFront(activity)
        }, completion: nil)
    }
    
    private var isActivityOn: Bool {
        for subview in view.subviews {
            if subview is ActivityView {
                return true
            }
        }
        return false
    }
    
    func removeActivity(animated: Bool) {
        for subview in view.subviews {
            if subview is ActivityView {
                UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    subview.removeFromSuperview()
                }, completion: nil)
            }
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func open(file name: String, ext: String) -> [[String: Any]]? {
        if let path = Bundle.main.path(forResource: name, ofType: ext) {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? [[String: Any]]
                return jsonResult
            } catch {
                print("Something went wrong")
            }
        }
        return nil
    }
    
}

