//
//  Realm+Ex.swift
//  OwoxTest
//
//  Created by Lora Kucher on 12/3/18.
//  Copyright © 2018 Lora Kucher. All rights reserved.
//

import UIKit
import RealmSwift

extension List {

    var array: [Object] {
        var a: [Object] = []
        for object in self {
            guard let rmObject = object as? Object
                else {
                    continue
            }
            a.append(rmObject)
        }
        return a
    }

    func show(size: Int) -> [Object] {
        var a: [Object] = []
        let arraySize = size > self.count ? self.count : size
        for i in 0..<arraySize {
            let object = self[i]
            if object is Object {
                guard let rmObject = object as? Object
                    else {
                        continue
                }
                a.append(rmObject)
            }
        }
        return a
    }

}

extension Realm {

    func filter<ParentType: Object>(parentType: ParentType.Type, subclasses: [ParentType.Type], predicate: NSPredicate) -> [ParentType] {
        return ([parentType] + subclasses).flatMap { classType in
            return Array(self.objects(classType).filter(predicate).sorted(byKeyPath: "time"))
        }
    }

}

extension Object {

    func writeBlock(_ block: (Realm) -> ()) {
        guard let realm = realm
            else {
                let realm = try! Realm()
                checkTransaction(for: realm, block)
                return
        }
        checkTransaction(for: realm, block)
    }

    private func checkTransaction(for realm: Realm, _ block: (Realm) -> ()) {
        if realm.isInWriteTransaction == true {
            block(realm)
        } else {
            try? realm.write {
                block(realm)
            }
        }
    }

    func removeSelf() {
        if realm == nil || isInvalidated == true {
            return
        } else {
            writeBlock { realm in
                realm.delete(self)
            }
        }
    }

}
