//
//  StepperView.swift
//  OwoxTest
//
//  Created by Lora Kucher on 12/2/18.
//  Copyright © 2018 Lora Kucher. All rights reserved.
//

import UIKit

class StepperView: UIView {
    
    @IBOutlet weak var counterLbl: UILabel!
    @IBOutlet weak var tintColorView: UIView!
    @IBOutlet private weak var plusOutlet: UIButton!
    @IBOutlet private weak var minusOutlet: UIButton!
    
    open var counter: Int = 1
    private var cornerRadios: CGFloat = 5
    private var color: UIColor = UIColor(red: 152 / 255, green: 204 / 255, blue: 247 / 255, alpha: 1)
    var pr: Product = Product()
    var price: Int = Int()
    
    static func configure(identifier: String = "StepperView", product: Product) -> StepperView {
        let view = Bundle.main.loadNibNamed(identifier, owner: nil, options: nil)?.first as? StepperView ?? StepperView()
        view.pr = product
        view.tintColorView.layer.cornerRadius = view.cornerRadios
        view.tintColorView.backgroundColor = view.color
        view.counter = product.count
        view.price = product.priceForOne
        view.counterLbl.text = String(product.count)
        view.plusOutlet.roundCorners([.bottomLeft, .topLeft], radius: view.cornerRadios)
        view.minusOutlet.roundCorners([.topRight, .bottomRight], radius: view.cornerRadios)
        return view
    }
    
    @IBAction func changeCount(_ sender: UIButton) {
        changeCounter(with: sender.tag)
        counterLbl.text = String(counter)
    }
    
    private func changeCounter(with tag: Int) {
        if counter <= 0 { counter = 1 }
        switch tag {
        case 1: counter -= 1
        case 2: counter += 1
        default: break
        }
        pr.counted = counter
        pr.countedPrice = price * counter
    }
    
}
