//
//  TotalView.swift
//  OwoxTest
//
//  Created by Lora Kucher on 12/3/18.
//  Copyright © 2018 Lora Kucher. All rights reserved.
//

import UIKit

class TotalView: UIView {

    @IBOutlet weak var checkoutOtlet: UIButton!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var count: UILabel!
    @IBOutlet weak var totalView: UIView!
    
    static func configure(identifier: String = "TotalView") -> TotalView {
        let view = Bundle.main.loadNibNamed(identifier, owner: nil, options: nil)?.first as? TotalView ?? TotalView()
        view.totalView.layer.borderColor = UIColor.lightGray.cgColor
        view.totalView.layer.borderWidth = 0.5
        view.checkoutOtlet.layer.cornerRadius = 8
        return view
    }
    
    @IBAction func checkOut(_ sender: UIButton) {
        UIApplication.topViewController()?.performSegue(withIdentifier: "showCheckout", sender: "")
    }
}
