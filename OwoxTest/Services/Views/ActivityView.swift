//
//  ActivityView.swift
//  OwoxTest
//
//  Created by Lora Kucher on 12/4/18.
//  Copyright © 2018 Lora Kucher. All rights reserved.
//

import UIKit

class ActivityView: UIView {
    
    static func configure(with size: CGSize) -> ActivityView {
        let view = Bundle.main.loadNibNamed("ActivityView", owner: nil, options: nil)?.first as? ActivityView ?? ActivityView()
        view.frame = CGRect(origin: CGPoint.zero, size: size)
        return view
    }
    
}
