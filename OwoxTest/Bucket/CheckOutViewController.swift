//
//  CheckOutViewController.swift
//  OwoxTest
//
//  Created by Lora Kucher on 12/5/18.
//  Copyright © 2018 Lora Kucher. All rights reserved.
//

import UIKit

class CheckOutViewController: UIViewController {

    @IBOutlet weak var txtView: UITextView!
    var productNames: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        title = NSLocalizedString("Заказ", comment: "")
        for product in ProductList.allProducts() {
            productNames.append(product.name ?? "")
        }
        txtView.text = productNames.joined(separator: ", \n \n")
    }

}
