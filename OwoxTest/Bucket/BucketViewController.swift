//
//  BucketViewController.swift
//  OwoxTest
//
//  Created by Lora Kucher on 12/2/18.
//  Copyright © 2018 Lora Kucher. All rights reserved.
//

import UIKit
import SDWebImage

enum State {
    
    case edit
    case bucket
    
    var title: String {
        switch self {
        case .bucket: return NSLocalizedString("Редакт.", comment: "")
        case .edit: return NSLocalizedString("Готово", comment: "")
        }
    }
    
}

class BucketViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalView: UIView!
    @IBOutlet weak var emptyView: UIView!
    
    private let cellHeight: CGFloat = 150
    private var icon: String? = String()
    private let refreshControl: UIRefreshControl = UIRefreshControl()
    let tView: TotalView = TotalView.configure()
    var cart: [Product] = []
    private var state: State = .bucket
    private var products = ProductList()
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    var link: URL? {
        guard let iconLink = icon else {
            return nil
        }
        return URL(string: iconLink)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addObservers()
        getCart()
        cart = ProductList.allProducts()
        checkEmptyState()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.addSubview(refreshControl)
        configureTotalView()
        tableView.register(UINib(nibName: "BucketTableViewCell", bundle: nil), forCellReuseIdentifier: "bucketTableViewCell")
    }
    
    @IBAction func edit(_ sender: UIBarButtonItem) {
        state = state == .bucket ? .edit : .bucket
        tableView.setEditing(state == .bucket ? false : true, animated: true)
        navigationItem.rightBarButtonItem?.title = state.title
        navigationItem.leftBarButtonItem?.title = state == .bucket ? NSLocalizedString("Добавить", comment: "") : ""
        navigationItem.leftBarButtonItem?.isEnabled = state == .bucket
    }
    
    @IBAction func addProduct(_ sender: UIBarButtonItem) {
        guard let dictionary = open(file: "products", ext: "json") else { return }
        addActivity()
        products.add(products.productList(with: dictionary[Int.random(in: 0 ..< dictionary.count)]))
        cart = ProductList.allProducts()
        checkEmptyState()
        configureTotalView()
    }
    
    // MARK: - Helper methods
    
    @objc private func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: .updateUI, object: nil)
    }
    
    @objc private func updateUI() {
        cart = ProductList.allProducts()
        configureTotalView()
    }
    
    private func getCart() {
        guard let dict = open(file: "cart", ext: "json") else { return }
        for one in dict {
           _ = products.productList(with: one)
        }
    }
    
    private func configureTotalView() {
        ProductList.count { count in
            self.tView.count.text = NSLocalizedString("\(count) \(self.correctCase()) на сумму:", comment: "")
        }
        ProductList.summ { summ in
            if let summ: Int = summ as? Int {
                self.tView.price.text = String(summ) + " грн"
                self.removeActivity(animated: true)
            }
        }
        totalView.addSubview(tView)
        tableView.reloadData()
    }
    
    private func correctCase() -> String {
        switch cart.count {
        case 1: return NSLocalizedString("товар", comment: "")
        case 2, 3, 4: return NSLocalizedString("товара", comment: "")
        default: return NSLocalizedString("товаров", comment: "")
        }
    }
    
    private func checkEmptyState() {
        emptyView.isHidden = cart.count != 0
    }

}

extension BucketViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cart.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: BucketTableViewCell = tableView.dequeueReusableCell(withIdentifier: "bucketTableViewCell") as? BucketTableViewCell else {
            return UITableViewCell()
        }
        let product = cart[indexPath.row]
        icon = product.imageURL
        cell.configure(product)
        cell.actualPriceLbl?.text = String(product.price)
        cell.nameLbl?.text = product.name
        cell.oldPriceLbl?.attributedText = product.oldPrice?.returnCrossedText()
        cell.imgView?.layer.borderWidth = 0.5
        cell.imgView?.layer.borderColor = UIColor.lightGray.cgColor
        cell.imgView?.sd_setImage(with: link, placeholderImage: UIImage(named: "placeholder"), options: .continueInBackground, completed: nil)
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            cart[indexPath.row].removeSelf()
            cart = ProductList.allProducts()
            checkEmptyState()
            configureTotalView()
            tableView.deleteRows(at: [indexPath], with: .left)
        }
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.reloadData()
    }
    
    @objc func refresh(_ sender: Any) {
        products.removeAll()
        getCart()
        cart = ProductList.allProducts()
        tableView.reloadData()
        configureTotalView()
        refreshControl.endRefreshing()
    }

}
